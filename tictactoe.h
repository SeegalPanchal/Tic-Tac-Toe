/* tictactoe.h
  Header File
  By: Seegal Panchal (panchals@uoguelph.ca)
  Student ID: 1016249
*/

#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* print and run a1p1 */
void debug(unsigned short us);
/* print and run a1p4 */
void debug2(unsigned short us);
/* print and run a1p5 */
void debug3(unsigned short us);

/* b3 --> us */
unsigned short b3tous(char b3[10]);
/* us --> b3 */
void b3fromus(char b3[10], unsigned short us);

/* b3 --> board */
void boardfromb3(char board[60], char b3[10]);
/* board --> b3 */
void boardtob3(char board[60], char b3[10]);

/* # of pieces on board */
char get_move(char b3[10]);
/* whos turn, X or O?*/
char get_turn(char b3[10]);

/* does the current board being played have a winner*/
char winner(unsigned short us);

/* can I move in this spot? is it occupied? */
unsigned short next (unsigned short us, char pos);

/* strucutre to store best move
reading and writing to file, etc...*/
struct strategy_struct {
    char best_move;
    char winner;
};

/* get a specific block in the stack */
void get_record(FILE *fp, unsigned short us, struct strategy_struct *record);
/* replace a specific block in the stack */
void set_record(FILE *fp, unsigned short us, struct strategy_struct record);

/* evaluate the best possible move */
void evaluate_move(FILE *fp, unsigned short us, struct strategy_struct *record);

/* player information */
char start();
/* player actually plays the game */
void play_game(char b3[10], unsigned short us, char player);

#endif
