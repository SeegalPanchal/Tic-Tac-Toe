#include "tictactoe.h"

/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: creates the AI (reads the best moves and chains them,
      so the computer knows the best move at all times)
Param:
  command line arguments
Return:
  0
*/
int main (int argc, char *argv[]) {
  /*
  i : the variable to run a for loop, can treat it as the us value I pass into other functions
  struct record: declaration of a temporary structure here
  FILE *fp: file pointer that I can use to write to a file
  */
  unsigned short i = 0;
  struct strategy_struct record;
  FILE *fp;
  fp = fopen("strategyfile", "rb+");
  /* open file in read binary +
  (+ means ability to write)*/
  if (argc != 2){
    fprintf( stderr, "Usage: %s [n]\n", argv[0] );
    exit(-1);
  } else {
    for (i = 0; i < 19683; i++) { /* loop through all boards */
      char b3[10];
      /* convert to base3 */
      b3fromus(b3, i);
      /* if the move # matches the inputted move*/
      if (get_move(b3) == atoi(argv[argc-1])+'0') {
        /* if winner is decided */
        if (winner(i) != ' ') {
          /*
            Writes that structure to
            strategyfile by calling set_record
          */
          record.best_move = -1;
          record.winner = winner(i);
          set_record(fp, i, record);
        } else {
          /* evaluate the best move and record them in the file */
          evaluate_move(fp, i, &record);
          set_record(fp, i, record);
        }
      }
    }
  }
  /* close the file stream */
  fclose(fp);
  return 0;
}
