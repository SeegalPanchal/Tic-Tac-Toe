#include "tictactoe.h"
/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: the actual tictactoe game
Param:
  none
Return:
  0
*/
int main () {
  /*
  b3[10]: the base 3 rep of the board
  player: the player ('1' or '2' --> 'O' or 'X')
  us: the board in base10 or decimal or 'normal' value
  */
  char b3[10], player;
  /* game hasn't started, us = 0*/
  unsigned short us = 0;
  player = start();
  /* start the game */
  play_game(b3, us, player);
  return 0;
}

/*
Desc: starts the game until it decides which side the player is playing on
Param:
  none
Return:
  char: the player variable intialized to the side he is playing on
*/
char start() {
  /*
  name: the name of the player (e.g. PROFESSOR FALKEN)
  side: the side they are playing (X/O)
  player: the side represented as '1' or '2'
  */
  char name[30], side[60], player = 0;

  printf("\nSHALL WE PLAY A GAME?\n");
  printf("\nPLEASE ENTER YOUR NAME: ");

  /* get the player name */
  fgets(name, sizeof(name)-1, stdin);

  /* flush any overflowing input from fgets */
  /* if new line dooesnt exist from previous fgets*/
  if (!strchr(name, '\n')) {
    while (fgetc(stdin)!='\n'); /* discard until new line */
  }

  printf("\nGREETINGS, %s\n", name);

  while (player != 'X' && player != 'x' && player != 'O' && player != 'o') {
      printf("Which side would you like to play (X/O)? ");
      /* get the side the player wants to play (X or O)*/
      fgets(side, sizeof(side)-1, stdin);
      /* if only 1 char was not inputted */
      if (strlen(side) != 2) {
        player = 0;
        printf("Input too long. ");
      }
      /* if the input was not a character (is this impossible? not sure )
      anyways, load it into the player char variable */
      if (sscanf(side, "%c", &player) != 1) {
        player = 0;
        printf("Input invalid. ");
        continue;
      }
      /* if they didn't input an X or an O*/
      if (player != 'X' && player != 'x' && player != 'O' && player != 'o') {
        player = 0;
        printf("Not an X/O. ");
        continue;
      }
  }
  /* switch format to 1's and 2's*/
  if (player == 'X' || player == 'x') {
    printf("OK. You will be X; I will be O.\n");
    player = '2';
  } else {
    printf("OK. You will be O; I will be X.\n");
    player = '1';
  }
  return player;
}

/*
Desc: handles running the actual game until the winner is decided, and prints the winner
Param:
  b3: the board in base 3 representation
  us: the board in decimal value
  player: the character of the side the player is playing on
Return:
  void
*/
void play_game(char b3[10], unsigned short us, char player) {
  /*
  b3: the base 3 representation of the board
  us: the board in 'normal', base 10 or decimal value
  player: the player '1' or '2'
  */
  char win = ' ';
  char comp;
  struct strategy_struct record;
  FILE *fp = fopen("strategyfile", "rb");
  /* while there is no winner */
  debug3(us);
  /* set the computer so we can
  check winner */
  if (player == '2') {
    comp = '1';
  } else {
    comp = '2';
  }
  while (win == ' ') {
    char turn;

    char player_input[64];
    int player_move = -1;

    char comp_move;
    /*start by printing empty board*/
    /* get base 3 of board */
    b3fromus(b3, us);
    /* get who's turn it is*/
    turn = get_turn(b3);
    /* if it is the players turn */
    if (turn == player) {
      /* playerMove = get char input */

      /* flush any overflowing input from fgets */
      /* if new line dooesnt exist from previous fgets*/
      if (!strchr(player_input, '\n')) {
        while (fgetc(stdin)!='\n'); /* discard until new line */
      }
      while (player_move == -1) {
        /*get the player input using fgets */
        printf("Your turn; choose a move [0-8]: ");
        fgets(player_input, sizeof(player_input)-1, stdin);
        if (strlen(player_input) != 2) {
          player_move = -1;
          printf("Input too long. ");
          /* force next iteration of loop */
          continue;
        }
        /* if 1 integer was not entered (0-9)*/
        if (sscanf(player_input, "%d", &player_move) != 1) {
          player_move = -1;
          printf("Input invalid. ");
          continue;
        }
        /* if the number is not between 0 and 8*/
        if (player_move < 0 || player_move > 8) {
          player_move = -1;
          printf("Input not in range. ");
          continue;
        }
        /* if number entered is not a legal move */
        if (next(us, player_move+'0') == 0) {
          player_move = -1;
          printf("Illegal move. ");
          continue;
        }
      }
      /* change us to the next board*/
      us = next(us, player_move+'0');
      /*check if a winner is there yet*/
      win = winner(us);
    } else { /* else it is the computers turn */
      get_record(fp, us, &record); /*check for the best possible move*/
      /*set the best move to the move the comp does*/
      comp_move = record.best_move+'0';
      printf("My turn; my move is %c\n", comp_move);
      /*set the next board and check for a winner*/
      us = next(us, comp_move);
      win = winner(us);
    }
    /*print the board*/
    debug3(us);
  }

  /* if the winner is the player */
  if (win == player) {
    printf("Congrats on winning!\n\n");
  } else if (win == comp) { /* lol the player can never win, but yeah when the comp wins*/
    printf("I won!\n\nA STRANGE GAME.\nTHE ONLY WINNING MOVE IS\nNOT TO PLAY.\n\n");
  } else { /* if the player ties (only other real possibility)*/
    printf("The game is a tie.\n\n");
  }
  return;
}
