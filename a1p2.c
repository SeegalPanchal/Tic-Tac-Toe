#include "tictactoe.h"
/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: creates an empty file where we write record into
(containing values winner = ' ', and best_move = -1)
Param:
  none
Return:
  0
*/
int main () {
  /*
  FILE *fp: file pointer so I can open a file stream and read/write to a file
  us i: so I can loop through all boards and pass is as an us value in other functions
  struct record: record variable where the best_move and winner are defaulted
  */
  FILE *fp;
  unsigned short i = 0;
  struct strategy_struct record;
  record.best_move = -1;
  record.winner = ' ';
  /* open a file stream in write binary mode */
  fp = fopen("strategyfile", "wb");
  if (fp != NULL) { /*if file opens properly*/
    for (i = 0; i < 19683; i++) { /* loop through every board */
      /* write the record to the file */
      fwrite(&record, sizeof(struct strategy_struct), 1, fp);
    }
  }
  /*close the file stream*/
  fclose(fp);
  return 0;
}
