#include "tictactoe.h"

/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: prints a1p1 and includes the best_move and winner
Param:
  command line arguments
Return:
  0
*/
int main (int argc, char *argv[]) {
  /* if the arguments is not exactly 2 (executing + inputing the board)*/
  if (argc != 2){
    fprintf( stderr, "Usage: %s [n]\n", argv[0] );
    exit(-1);
  } else {
    /*call debug
    take the last argument
    make the user input a short
    debug with the user input*/

    /* debug 1 pre day-8
    debug(atoi(argv[argc-1]));
    */
    debug2(atoi(argv[argc-1]));
  }

  return 0;
}
