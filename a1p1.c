#include "tictactoe.h"
/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: create and draw a board
Param:
  command line arguments --> the board number in us value
Return:
  0
*/
int main (int argc, char *argv[]) {

  if (argc != 2){
    fprintf( stderr, "Usage: %s [n]\n", argv[0] );
    exit(-1);
  } else {
    /*call debug
    take the last argument
    make the user input a short
    debug with the user input*/
    debug(atoi(argv[argc-1]));
  }

  return 0;
}
