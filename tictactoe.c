#include "tictactoe.h"
/*
Name: Seegal Panchal
Date: Jan 29 2018
ID: 1016249

Desc: the main tictactoe file containing functions that can be universally called and be useful
*/

/*
Desc: debug (print the board, a winner if there is one, and the b3 and us values)
Param:
  us value: the unsigned short of the board which is converted into b3
Return:
  void
*/
void debug(unsigned short us) {
  /*
  char b3: base 3 representation of the board
  board[60]: the array holding the actual board being printed
  pos: the pos on the board [0-8]
  */
  char b3[10], board[60], pos;
  int i;

  /* convert unsigned short to base 3 representation */
  b3fromus(b3, us);

  /* print board number*/
  printf("Board Number = %d\n", us);

  /* print board in base3*/
  printf("b3: %s\n", b3);

  /* print board */
  printf("Board pic:");
  boardfromb3(board, b3);

  /* print move number */
  printf("Move: %c\n", get_move(b3));
  /* print turn number*/
  printf("Turn: %c\n", get_turn(b3));

  /* print winner */
  printf("Winner: %c\n", winner(us));

  /* run through for loop,
  print board # for possible move*/
  i = 0;
  for (i = 0; i < 9; i ++) {
    pos = i + '0'; /* convert pos to a char */
    printf("%c -> %d\n", pos, next(us, pos));
  }
}

/*
Desc: debug2 (print the board, a winner if there is one, and the b3 and us values)
      also print the best_move and winner read from the strategyfile
Param:
  us value: the unsigned short of the board which is converted into b3
Return:
  void
*/
void debug2(unsigned short us) {
  /*
  char b3: base 3 representation of the board
  board[60]: the array holding the actual board being printed
  pos: the pos on the board [0-8]
  int i: to print all the possible next moves
  struct record: to read from the FILE
  FILE *fp: to open a stream to strategyfile to read from
  */
  char b3[10], board[60], pos;
  int i;
  struct strategy_struct record;
  FILE *fp;

  b3fromus(b3, us);

  /* print board number*/
  printf("Board Number = %d\n", us);

  /* print board in base3*/
  printf("b3: %s\n", b3);

  /* print board */
  printf("Board pic:");
  boardfromb3(board, b3);

  /* print move number */
  printf("Move: %c\n", get_move(b3));
  /* print turn number*/
  printf("Turn: %c\n", get_turn(b3));

  /* print winner */
  printf("Winner: %c\n", winner(us));

  /* print best_move and winner*/
  /* remove the warning that fp is not initialized*/

  fp = fopen("strategyfile", "rb");
  get_record(fp, us, &record);
  printf("best_move=%d, ", record.best_move);
  printf("winner=%c\n", record.winner);
  fclose(fp);

  /* run through for loop,
  print board # for possible move*/
  i = 0;
  for (i = 0; i < 9; i ++) {
    pos = i + '0'; /* convert pos to a char */
    printf("%c -> %d\n", pos, next(us, pos));
  }
}

/*
Desc: debug3 easy way to print the board in a1p5
Param:
  us value: the unsigned short of the board which is converted into b3
Return:
  void
*/
void debug3(unsigned short us) {
  /*
  b3[10]: board in base 3 representation
  board[60]: array holding the board that is printed to screen
  */
  char b3[10], board[60];

  /* convert us to base 3 rep */
  b3fromus(b3, us);
  /* convert base 3 rep to board, then print board */
  boardfromb3(board, b3);
  /* new line */
  printf("\n");
}

/*
Desc: b3tous gets a base 3 representation of the board and converts it into decimal format
Param:
  b3[10]: the base 3 representation to convert to us
Return:
  unsigned short: returns the decimal (base10) version of the base 3 array passed in
*/
unsigned short b3tous(char b3[10]) {
  /*
  unsigned short us: the board in base 10
  unsigned short count: variable used to convert from base 3 rep
  */
  unsigned short us;
  unsigned short count;
  int i;

  us = 0;
  count = 1;
  i = 0;
  /* loop from 8 to 0 and each time collect the b3 value,
  and raise it to the power of (multiple it by 3^x)*/
  for (i = 8; i >= 0; i --) {
    unsigned short temp = b3[i] - '0';
    us = us + (count * temp);
    count *= 3;
  }
  /* return the board in base 10*/
  return us;
}

/*
Desc: convert an unsigned short into base 3 representation
Param:
  us value: the unsigned short of the board which is converted into b3
Return:
  void (doesnt need to return because b3 is an array and it just changes that)
*/
void b3fromus(char b3[10], unsigned short us ) {
  /*
  int i: used to loop through a for loop (the b3 array)
  unsigned short temp: used to hold the remainder when calculating b3
  */
  int i;
  unsigned short temp;

  i = 8;
  temp = 0;
  b3[9] = '\0';

  /* loop through the array, and store the remainder
  the remainder goes in the rightmost spot, then divide by 3
  and put the remainder int he righmost open spot again, continue
  until fully converted*/
  for (i = 8; i >= 0; i--) {
    temp = us % 3;
    us = us / 3;
    b3[i] = temp + '0';
  }
  /* void (formal) return */
  return;
}


/*
Desc: takes a base 3 representation of the board and prints the board out onto the screen
Param:
  char board[60]: the board that is being printed out
  char b3[10]: the base 3 representation of the board
Return:
  void
*/
void boardfromb3(char board[60], char b3[10]) {
  /*
  index1: the spot where X and O should be appearing
  index2: the adjustment for the board ( 4th spot is far away from 3rd spot )
  i: for looping through the b3 array
  */
  int index1, index2, i;

  /*copy the board into the board string*/
  strcpy(board, "   |   |   \n---+---+---\n   |   |   \n---+---+---\n   |   |   \0");

  index1 = 0;
  index2 = 0;
  i = 0;
  /* loop through b3, and fill the spots with the X's and O's*/
  for (i = 0; i < 9; i++) {
    /* index1 = 3*i (3 increment) + (additional for spaces and |) */
    index1 = 3*i+i+1;
    /* index2 = 12 * the row its on (1st row, no adjustment, 2nd row, 12 adjustment, etc)*/
    index2 = (i/3) * 12;
    switch (b3[i]) {
      case '0':
        board[index1+index2] = ' ';
        break;
      case '1':
        board[index1+index2] = 'O';
        break;
      case '2':
        board[index1+index2] = 'X';
        break;
      default:
        fprintf(stderr, "Error: bad value in b3\n");
        exit(-1);
        break;
    }
  }
  /*print the board here*/
  printf("\n%s\n", board);
  /* void (formal) return */
  return;
}

/*
Desc: looks at the board, and converts that into a base3 representation
Param:
  char board[60]: the whole drawn out board as a character
  char b3[10]: this is where the converted board is stored
Return:
  void
*/
void boardtob3(char board[60], char b3[10]) {
  /*
  index1: increments through where each X and O should be
  index2: adjusts for the board inside the array (that is not spaces or X's or O's)
  i: used for the 'for loop'
  */
  int index1, index2, i;

  index1 = 1;
  index2 = 0;
  i = 0;
  for (i = 0; i < 9; i++) {
    /*look through the board where there should be an X or O or space
    and depending on what it is, populate the b3 array */
    switch (board[index1+index2]) {
      case ' ':
        b3[i] = '0';
        break;
      case 'O':
        b3[i] = '1';
        break;
      case 'X':
        b3[i] = '2';
        break;
      default:
        fprintf(stderr, "Error: bad value in board?\n");
        exit(-1);
        break;
    }
    /*increment the indices*/
    index1 += 4;
    index2 = ((i+1)/3) * 12;
  }
  /* print the b3 you created */
  printf("Board to b3: %s\n", b3);
  /* void (formal) return */
  return;
}

/*
Desc: loop through a b3 array, if the entry is non-zero, its a piece. Count the number of pieces
      and return that as the move number
Param:
  b3[10]: takes the b3 representation of the board
Return:
  char: returns the move number as a character
*/
char get_move(char b3[10]) {
  int i, pieces;
  i = 0;
  pieces = 0;
  /*loop through b3, count the number of non-zero entries, those are X's and O's*/
  for (i = 0; i < 9; i++) {
    if (b3[i] != '0') {
      pieces++;
    }
  }
  /* return the move number
    I know you dont have to convert to an int,
    but this was before your FAQ
    and changing it now will probably break it
    */
  return (pieces+'0'); /* return character of move #*/
}

/*
Desc: get the move number, and then gets the remainder when devided by 2
      if its X, X plays on move 0, 2, 4, etc... therefore move % 2 == 0 should be X
      and otherwise it must be O
Param:
  char b3: take the b3 array into it, so we can get the number of moves
Return:
  char: the turn number as a character '1' for O and '2' for X
*/
char get_turn(char b3[10]) {
  /*
  moveNum: the move number, modulus this (divide is --> get remainder) to see if X's or O's turn
  */
  int moveNum;
  moveNum = get_move(b3) - '0';

  /* if the value is 2 in base 3
  it is 'X's turn, return 2 */
  if (moveNum % 2 == 0) {
    return '2';
  } else { /* else return 1 for 'O's turn */
    return '1';
  }
}

/*
Desc: checks if the current board has a winner
Param:
  unsigned short us: the unsigned short of the board
Return:
  char: the winner (returns '0', '1', '2' --> if the board has a winner)
                   (returns ' ' --> space if board is incomplete)
*/
char winner(unsigned short us) {
  /*
  char b3: the base3 representation of the board
  char winner: who wins this board?
  */
  char b3[10], winner;
  b3fromus(b3, us);

  /* default winner is tie*/
  winner = ' ';

  /*
  Winning base 3 board representation:
  012
  345
  678
  036
  147
  258
  048
  246
  */

  /* I don't know if you are supposed to use else if
  but it looks like the prof used If so im going to use that */

  /* check if X has won
  do this by checking the above board combinations*/
  if (b3[0] == '2' && b3[1] == '2' && b3[2] == '2') {
    winner = '2';
  }
  if (b3[3] == '2' && b3[4] == '2' && b3[5] == '2') {
    winner = '2';
  }
  if (b3[6] == '2' && b3[7] == '2' && b3[8] == '2') {
    winner = '2';
  }
  if (b3[0] == '2' && b3[3] == '2' && b3[6] == '2') {
    winner = '2';
  }
  if (b3[1] == '2' && b3[4] == '2' && b3[7] == '2') {
    winner = '2';
  }
  if (b3[2] == '2' && b3[5] == '2' && b3[8] == '2') {
    winner = '2';
  }
  if (b3[0] == '2' && b3[4] == '2' && b3[8] == '2') {
    winner = '2';
  }
  if (b3[2] == '2' && b3[4] == '2' && b3[6] == '2') {
    winner = '2';
  }

  /* check if O has won
  do this by checking the above board combinations*/
  if (b3[0] == '1' && b3[1] == '1' && b3[2] == '1') {
    winner = '1';
  }
  if (b3[3] == '1' && b3[4] == '1' && b3[5] == '1') {
    winner = '1';
  }
  if (b3[6] == '1' && b3[7] == '1' && b3[8] == '1') {
    winner = '1';
  }
  if (b3[0] == '1' && b3[3] == '1' && b3[6] =='1') {
    winner = '1';
  }
  if (b3[1] == '1' && b3[4] == '1' && b3[7] == '1') {
    winner = '1';
  }
  if (b3[2] == '1' && b3[5] == '1' && b3[8] == '1') {
    winner = '1';
  }
  if (b3[0] == '1' && b3[4] == '1' && b3[8] == '1') {
    winner = '1';
  }
  if (b3[2] == '1' && b3[4] == '1' && b3[6] == '1') {
    winner = '1';
  }

  /* check if the board is full and it is a tie*/
  if (get_move(b3) == '9' && winner == ' ') {
    winner = '0';
  }

  /* return the winner character */
  return winner;
}

/*
Desc: next function returns the next board if you placed a piece at the location you inputted
Param:
  unsigned short us: the current board you are on in base10
  char pos: where you are going to place the piece
Return:
  unsigned short: the base10 value of the next board if you place a piece there
*/
unsigned short next (unsigned short us, char pos) {
  /*
  char b3: used to check if the position is occupied (if spot on b3 array isnt 0)
  */
  char b3[10];
  b3fromus(b3, us);
  /* if the boord has a piece
  at that position, return 0 */
  if (b3[pos - '0'] != '0') {
    return 0;
  } else { /* else put a piece at that position */
    /* check whos turn it is*/
    int turn = get_turn(b3) - '0';

    /* if X's turn, place X a the position*/
    if (turn % 2 == 0) {
      b3[pos - '0'] = '2';
    } else { /* else place a O at the position */
      b3[pos - '0'] = '1';
    }
    return b3tous(b3); /* return the us of the new board */
  }
}

/*
Desc: takes a record, and seeks the position in the file (which record are we replacing?)
      and replaces that record
Param:
  FILE *fp = file pointer so I dont have to open another file stream here as well
  us value: the unsigned short of the board
  struct record: reference of record structure to read from file
Return:
  void
*/
void get_record(FILE *fp, unsigned short us, struct strategy_struct *record) {
  if (fp != NULL) {
    /* seek the position where we want to read */
    fseek(fp, sizeof(struct strategy_struct)*us, SEEK_SET);
    /* read the block there and record it into our record*/
    fread(record, sizeof(struct strategy_struct), 1, fp);
  }
  /* return void */
  return;
}

/*
Desc: takes a record, and seeks the position in the file (which record are we replacing?)
      and replaces that record
Param:
  FILE *fp = file pointer so I dont have to open another file stream here as well
  us value: the unsigned short of the board
  struct record: value of record structure to write into file
Return:
  void
*/
void set_record(FILE *fp, unsigned short us, struct strategy_struct record) {
  /*if the file works (reads and can write, isn't NULL)*/
  if (fp != NULL) {
    /* seek the position where we want to write */
    fseek(fp, (sizeof(struct strategy_struct))*us, SEEK_SET);
    /* replace the block there with our new record block */
    fwrite(&record, sizeof(struct strategy_struct), 1, fp);
  }
}

/*
Desc: reads the file and checks the best possible move that will lead to a winner or a tie
Param:
  FILE *fp = file pointer so I dont have to open another file stream here as well
  unsigned short us = the us value of the board
  struct *record = pointer to a struc
Return:
  void
*/
void evaluate_move(FILE *fp, unsigned short us, struct strategy_struct *record) {
  /*
  b3: base 3 rep of the board
  int i: looping through the positions on the board
  int tie_pos: the position that lead to a tie
  int found_tie: a tie was found
  int legal_move: this move is possible, not a great move through
  struct find: temp truct to find winner and best_move
  unsigned short highest_value: highest legal number of the board possible
  */
  char b3[10];
  int i, tie_pos, found_tie, legal_move;
  struct strategy_struct find;

  /* checking for highest value of US in loop*/
  unsigned short highest_value = 0;

  i = 0;
  tie_pos = 9;
  found_tie = 0;
  legal_move = -1;

  /*interate over the possible boards*/
  for (i = 0; i < 9; ++i) {
    unsigned short next_board;
    char pos = i + '0';
    next_board = next(us, pos);
    /* if the next move is legal */
    if (next_board != 0) {
      /* get the record and read into
      the temp struct we created*/
      get_record(fp, next_board, &find);
      /* get the base 3, use this to check
      who's turn it is */
      b3fromus(b3, us);
      /* if whoever's turn it is, matches the
      winner on the next board */
      if (get_turn(b3) == find.winner) {
        /* set the new winner and best move
        and return from the function*/
        record->winner = find.winner;
        record->best_move = i;
        /* return out of the function */
        return;
      } else if (find.winner == '0') {
        /* if its a tie, set the tie position
        to a temporary variable */
        /* if so I can get highest block
        pos for the computer*/
        if (highest_value < next_board) {
          highest_value = next_board;
          tie_pos = i;
        }
        /* set a variable found_tie to 1 */
        found_tie = 1;
      } else {
        /* else set a third temp variable
        legal_move to the position that let
        to the current legal move */
        legal_move = i;
      }
    }
  }
  /* check if there was a tie, if there was */
  if (found_tie == 1) {
    /* set winner to tie, and best move
    to the one that leads to the tie */
    record->winner = '0';
    record->best_move = tie_pos;
  } else { /* if no tie was found
    set best move to any legal_move and
    set winner to opponent*/
    if (get_turn(b3) == '1') {
      record->winner = '2';
      record->best_move = legal_move;
    } else {
      record->winner = '1';
      record->best_move = legal_move;
    }
  }
  /* void (formal) return */
  return;
}
